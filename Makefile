#Wlasny plik Makefile

FLAGS = -c -g -Iinc -Wall -pedantic
FINAL = -Wall -pedantic

output: main.o Wektor.o Macierz.o UkladRownan.o
	g++ ${FINAL} main.o Wektor.o Macierz.o UkladRownan.o -o uklad_rownan
	rm *.o

	#	PLik makefile automatycznie otworzy program
	./uklad_rownan < rownanie_liniowe.dat

main.o: src/main.cpp
	g++ ${FLAGS} src/main.cpp

UkladRownan.o: src/UkladRownan.cpp inc/UkladRownan.hh
	g++ ${FLAGS} src/UkladRownan.cpp

Macierz.o: src/Macierz.cpp inc/Macierz.hh
	g++ ${FLAGS} src/Macierz.cpp

Wektor.o: src/Wektor.cpp inc/Wektor.hh inc/Rozmiar.hh
	g++ ${FLAGS} src/Wektor.cpp