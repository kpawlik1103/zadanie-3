#ifndef UKLADROWNAN_HH
#define UKLADROWNAN_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "Macierz.hh"
#include <iostream>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

class UkladRownan
{
    private:

        Macierz dataMacierz;
        Wektor  dataWektor;
        Wektor  rozwiazanie;

    public:

        //  KOnstruktor
        UkladRownan();

        //  Wczytywanie ukladu
        friend istream& operator >> (istream&, UkladRownan&);

        //  Wyswietlanie ukladu
        friend ostream& operator << (ostream&, UkladRownan&);

        //  Oblicza rozwiazanie ukladu
        void ObliczRozwiaznie();

        //  Zwraca rozwiazanie ukladu
        Wektor ZwrocRozwiazanie() const;

        //  Oblicza i zwraca blad ukladu
        Wektor ZwrocBladUkladu();
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream&, UkladRownan&);

ostream& operator << (ostream&, UkladRownan&);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#endif