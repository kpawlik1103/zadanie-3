#ifndef MACIERZ_HH
#define MACIERZ_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "Wektor.hh"
#include <iostream>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

class Macierz
{
    private:

        Wektor kolumna[ROZMIAR];

    public:

        //  Konstruktor
        Macierz();

        //  Wczytywanie macierzy
        friend istream& operator >> (istream&, Macierz&);

        //  Wyswietlanie macierzy
        friend ostream& operator << (ostream&, Macierz&);

        //  Oblicza i zwraca wyznacznik macierzy
        double ZwrocWyznacznik() const;

        //  Transponowanie macierzy
        Macierz Transponuj() const;

        //  Operator funkcyjny
        const double operator () (int, int) const;

        //  Zamiana kolumn
        void ZamienKolumny(int, Wektor);

        //  Potegowanie macierzy
        void Poteguj(int);

        //  Mnozenie macierzy
        Wektor operator * (Wektor);

        //  Potegowanie macierzy
        Macierz operator ^ (int);
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream&, Macierz&);

ostream& operator << (istream&, Macierz&);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */
#endif