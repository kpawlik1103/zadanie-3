#ifndef WEKTOR_HH
#define WEKTOR_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "Rozmiar.hh"
#include <iostream>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

class Wektor
{
    private:

        double wartosc[ROZMIAR];

    public:

        //  Konstruktor
        Wektor(bool = true);

        //  Wczytywanie wektora
        friend istream& operator >> (istream&, Wektor&);

        //  Wyswietlanie wektora
        friend ostream& operator << (ostream&, Wektor&);

        //  Obliczanie i zwracanie dlugosci wektora
        double ZwrocDLugosc() const;

        //  Przypisywanie wartosci do indeksu wektora
        void UstawWartosc(int, double);

        //  Dodawanie wektorow
        Wektor operator + (Wektor);

        //  Odejmowanie wektorow
        Wektor operator - (Wektor);

        //  Mnożenie wektora przez liczbe
        Wektor operator * (double);

        //  Dzielenie wektora przez liczbe
        Wektor operator / (double);

        //  Iloczyn skalarny
        double operator * (Wektor);

        //  Iloczyn wektorowy
        Wektor operator % (Wektor);

        //  Zwracanie wartosci z wektora
        double operator [] (int) const;
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream&, Wektor&);

ostream& operator << (ostream&, Wektor&);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#endif