#include "Wektor.hh"
#include <iostream>
#include <cmath>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

Wektor::Wektor(bool pusty)
{

    /*
        Konstruktor

        Konstruktor domyślnie jest pusty, tzn. nie przyjmuje zadnych wartosci wstepnych.
        Istnieje mozliwosc utworeznia wektora zerowego od razu przy jesto tworzeniu.
        W tym celu nalezy jako parametr funkcyjny uzyc slowa kluczowego false.
    */

    if(pusty == false)
    {
        for(int i = 0; i < ROZMIAR; i++)
        {
            wartosc[i] = 0;
        }
    }
}

double Wektor::ZwrocDLugosc() const
{

    /*
        Funkcja oblicza i zwraca dlugosc wektora w postaci zmiennej typu double.
    */

    double tmp = 0;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp =+ tmp + pow(wartosc[i], 2);
    }

    return sqrt(tmp);
}

void Wektor::UstawWartosc(int indeks, double data)
{

    /*
        Funkcja sluzy zmianie konkretnej wartosci wektora. W tym celu nalezy podac
        indeks komorki do zmiany oraz nowa wartosc jej przypisywana.
    */

    wartosc[indeks] = data;
}

Wektor Wektor::operator + (Wektor data)
{
    Wektor tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.wartosc[i] = wartosc[i] + data.wartosc[i];
    }

    return tmp;
}

Wektor Wektor::operator - (Wektor data)
{
    Wektor tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.wartosc[i] = wartosc[i] - data.wartosc[i];
    }

    return tmp;
}

Wektor Wektor::operator * (double data)
{
    Wektor tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.wartosc[i] = wartosc[i] * data;
    }

    return tmp;
}

Wektor Wektor::operator / (double data)
{
    Wektor tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.wartosc[i] = wartosc[i] / data;
    }

    return tmp;
}

double Wektor::operator * (Wektor data)
{
    double tmp = 0;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp = tmp + wartosc[i] * data.wartosc[i];
    }

    return tmp;
}

Wektor Wektor::operator % (Wektor data)
{
    Wektor tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.wartosc[i] = wartosc[(i + 1) % 3] * data.wartosc[(i + 2) % 3] - wartosc[(i + 2) % 3] * data.wartosc[(i + 1) % 3];
    }

    return tmp;
}

double Wektor::operator [] (int data) const
{
    return wartosc[data];
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream& strWe, Wektor& data)
{

    /*
        Funkcja wczytuje dane do wektora. Ma do nich dostep, poniewaz jest ona
        zaprzyjazniona z klasa Wektor. Jest to domyslny sposob wczytwania dancyh
        tej klasy.
    */

    for(int i = 0; i < ROZMIAR; i++)
    {
        cin >> data.wartosc[i];
    }

    return strWe;
}

ostream& operator << (ostream& strWy, Wektor& data)
{

    /*
        Funkcja wyswietla wketor w postaci oczekiwanej przez uzytkownika. Jest to
        funkcja zaprzyjazniona z klasa Wektor, wiec posiada ona dostep do jego
        prywantych atrybutow. Jest to domyslny sposob wyswietlania wektora, z
        dodatkowymi nawiasami oraz przecinkami ulatwiajacymi odczyt danych.
    */
   
    cout << "( ";

    for(int i = 0; i < ROZMIAR; i++)
    {
        cout.width(7);
        cout << data.wartosc[i];
        if(ROZMIAR - i == 1)    cout << " )";
        else                    cout << ", ";
    }

    return strWy;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */