#include "Macierz.hh"
#include <iostream>
#include <cmath>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

Macierz::Macierz()
{

}

double Macierz::ZwrocWyznacznik() const
{

    /*
        Funkcja oblicza i zwraca wyznacznik macierzy w postaci zmiennej typu double.
        Funkcja nie zmienia zadnych wartosci macierzy.
    */

    double tmp = 0;

    if(ROZMIAR == 2)
    {
        tmp = kolumna[0][0] * kolumna[1][1] - kolumna[0][1] * kolumna[1][0];
        return tmp;
    }

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp = tmp + kolumna[i][0] * kolumna[(i + 1) % ROZMIAR][1 % ROZMIAR] * kolumna[(i + 2) % ROZMIAR][2 % ROZMIAR];
        tmp = tmp - kolumna[i][2 % ROZMIAR] * kolumna[(i + 1) % ROZMIAR][1 % ROZMIAR] * kolumna[(i + 2) % ROZMIAR][0];
    }

    return tmp;
}

Macierz Macierz::Transponuj() const
{

    /*
        Funkcja oblicza i zwraca macierz transponowana.

        UWAGA!
        Funkcja nie transponuje macierzy na ktorej dziala!

        Jest to spowodowane tym, by nie naruszac danych wewnetrzych macierzy.
        W celu transponowania bierzacej macierzy nalezy uzyyc nastepuajacej formuly:

        macierz = macierz.Transponuj();
    */

    Macierz tmp;

    for(int i = 0; i < ROZMIAR; i++)
    {
        for(int j = 0; j < ROZMIAR; j++)
        {
            double trsp;
            trsp = kolumna[j][i];
            tmp.kolumna[i].UstawWartosc(j, trsp);
        }
    }

    return tmp;
}

const double Macierz::operator () (int wie, int kol) const
{

    /*
        Funkcja zwraca wartosc konkretnej wartosci z danego wiersza,
        z danej kolumny.
    */

    return kolumna[kol][wie];
}

void Macierz::ZamienKolumny(int indeks, Wektor data)
{

    /*
        Funkcja zamienia kolumne o zadeklarowanym indeksie z wektorem,
        ktory zostanie podany.
    */

    if(indeks < ROZMIAR)
    {
        kolumna[indeks] = data;
    }
    else
    {
        cout << "Blad! Bledny indkes kolumny do zamiany." << endl;
    }
    
}

void Macierz::Poteguj(int data)
{
    for(int i = 0; i < ROZMIAR; i++)
    {
        for(int j = 0; j < ROZMIAR; j++)
        {
            double potega = pow(kolumna[i][j], data);
            kolumna[i].UstawWartosc(j, potega);
        }
    }
}

Wektor Macierz::operator * (Wektor data)
{
    Wektor tmp;

    for (int i = 0; i < ROZMIAR; i++)
    {
        double newValue;

        newValue = data * kolumna[i];

        tmp.UstawWartosc(i, newValue);
    }

    return tmp;    
}

Macierz Macierz::operator ^ (int potega)
{
    Macierz tmp;
    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp.kolumna[i] = this->kolumna[i];  
    }

    tmp.Poteguj(potega);

    return tmp;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream& strWe, Macierz& data)
{

    /*
        Funkcja wczytuje dane do macierzy. Ma do nich dostep, poniewaz jest ona
        zaprzyjazniona z klasa Macierz. Jest to domyslny sposob wczytwania dancyh
        tej klasy.
    */

    for(int i = 0; i < ROZMIAR; i++)
    {
        cin >> data.kolumna[i];
    }

    return strWe;
}

ostream& operator << (ostream& strWy, Macierz& data)
{

    /*
        Funkcja wyswietla macierz w postaci oczekiwanej przez uzytkownika. Jest to
        funkcja zaprzyjazniona z klasa Macierz, wiec posiada ona dostep do jego
        prywantych atrybutow. Jest to domyslny sposob wyswietlania macierzy, z
        dodatkowymi nawiasami oraz przecinkami ulatwiajacymi odczyt danych.
    */

    for(int i = 0; i < ROZMIAR; i++)
    {
        cout << data.kolumna[i] << endl;
    }

    return strWy;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */