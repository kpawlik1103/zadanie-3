#include "UkladRownan.hh"
#include <iostream>
#include <iomanip>

using namespace  std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

int main()
{
    cout << setprecision(4);

    //  Tworzenie ukladu rownan
    UkladRownan uklad;
    Wektor rozwiazanie;
    Wektor blad;

    //  Wczytywanie danych do ukladu i wyswietlanie
    cin >> uklad;
    cout << uklad << endl;

    //  Oblicza rozwiazanie ukladu
    uklad.ObliczRozwiaznie();
    rozwiazanie = uklad.ZwrocRozwiazanie();
    
    //  Wyswietlanie rozwiazania ukladu
    cout << "Rozwiazanie x = (x1, x2, x3):" << endl;
    if(rozwiazanie.ZwrocDLugosc() != 0)    cout << rozwiazanie << endl;
    else    cout << "Ukad jest sprzeczny badz nieoznaczony." << endl;

    cout << endl;

    //  Obliczanie i wyswietlanie bledu ukladu
    blad = uklad.ZwrocBladUkladu();

    cout.width(25);
    cout << "Wektor bledu:";

    cout.width(12);
    cout << "   Ax-b   = " << blad  << endl;

    //  Obliczanie i wyswietlanie dlugosci bledu ukladu
    cout.width(25);
    cout << "Dlugosc wektora bledu:";

    cout << setprecision(15);
    cout.width(12);
    cout << "||Ax-b|| = " << blad.ZwrocDLugosc() << endl;

    cout << endl;

    return 0;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */