#include "UkladRownan.hh"
#include <iostream>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

UkladRownan::UkladRownan()
{
    
}

void UkladRownan::ObliczRozwiaznie()
{

    /*
        Funkcja oblicza rozwiazanie wykladu. Jesli ono istnieje jest zapisywane w
        postaci nowego wektora. W przypadku rownan nieoznaczonych lub sprzecznych
        zapisuje ona wektor zerowy, co oznacza niemoznosc znalezienia rozwiazania.
        Funkcja powinna dzialac wlasnie w ten spoosb, by w wypadku ukladu sprzecznego
        nie zwrocono blednego wektora. Ponadto niemoznosc rozstrzygniecia rozwiazania
        w programie definiuje dlugosc wektora rowna zeru. Rozwiazanie to nie wyklucza
        jednak rozszerzenia do rozstrzygania o sprzecznosci lub nieoznaczonosci ukladu.
    */

    Wektor tmp(false);
    double wyznacznikGlowny = dataMacierz.ZwrocWyznacznik();

    //  Musi istniec rozwiazanie
    if(wyznacznikGlowny != 0)
    {
        double wartosc;

        for(int i = 0; i < ROZMIAR; i++)
        {
            Macierz TMP = dataMacierz;
            TMP.ZamienKolumny(i, dataWektor);

            wartosc = TMP.ZwrocWyznacznik() / wyznacznikGlowny;
            tmp.UstawWartosc(i, wartosc);
        }
    }

    /*
        Jesli wyznacznik glowny macierzy rowny jest zero,
        to uklad jest sprzeczny badz nieoznaczony.
    */

    rozwiazanie = tmp;
}

Wektor UkladRownan::ZwrocRozwiazanie() const
{
    return rozwiazanie;
}

Wektor UkladRownan::ZwrocBladUkladu()
{

    /*
        Funkcja oblicza i zwraca blad ukladu w postaci wektora.
    */
   
    Wektor tmp;
    Macierz TMP;

    TMP = dataMacierz.Transponuj();

    tmp = TMP * rozwiazanie - dataWektor;

    return tmp;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

istream& operator >> (istream& strWe, UkladRownan& data)
{
    
    /*
        Funkcja wczytuje dane do ukladu. Ma do nich dostep, poniewaz jest ona
        zaprzyjazniona z klasa UkladRownan. Jest to domyslny sposob wczytwania dancyh
        tej klasy.
    */

    cin >> data.dataMacierz;
    cin >> data.dataWektor;
    return strWe;
}

ostream& operator << (ostream& strWy, UkladRownan& data)
{

    /*
        Zakomentowana wersja ponizej sluzy jako backup. Jest to stara funkca wyswietlajaca
        uklad rownan. W wypadku problemow z wesja rozszerzona zawsze mozna do niej wrocic.

        Wersja niezakomentowana wyswietla uklad w postaci transponowanej oraz z odpowiednimi
        znakami. Jest to rozszerzenie do calej pracy. W wypadku bledow z wyswietlaniem,
        wciaz pozostaje wersja uprosczona.
    /*

    cout << "Macierz A^T:" << endl;
    cout << data.dataMacierz << endl;

    cout << "Wektor wyrazow wolnych b:" << endl;
    cout << data.dataWektor << endl;
    */

    for(int i = 0; i < ROZMIAR; i++)
    {
        cout << endl;
        cout << " | ";

        for(int j = 0; j < ROZMIAR; j++)
        {
            //  Wyswietla macierz transponowana
            cout.width(8);
            cout << data.dataMacierz(i, j);
        }

        cout << " || "  << 'x' << i + 1 << " | ";

        if(i == ROZMIAR / 2)    cout << '=';
        else                    cout << ' ';

        cout << " | ";
        cout.width(5);
        cout << data.dataWektor[i];
        cout << " | ";
    }

    cout << endl;
    
    return strWy;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */